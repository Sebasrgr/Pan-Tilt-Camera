/*
 * Copyright 2016-2019 NXP
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of NXP Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/**
 * @file    TestProject.c
 * @brief   Application entry point.
 */
#include <stdio.h>
#include "board.h"
#include "peripherals.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "MK64F12.h"
#include "fsl_debug_console.h"
#include "fsl_adc16.h"
#include "fsl_ftm.h"
#include "fsl_dspi.h"

// Definition Macros

#define TRANSFER_BAUDRATE 2000000
#define PIXY_TYPE_RESPONSE_ERROR 			0x03
#define CCC_RESPONSE_BLOCKS                 0x21
#define CCC_REQUEST_BLOCKS                  0x20

#define FRAME_WIDTH	(316U)
#define FRAME_HEIGHT (208U)

#define CENTER_WIDTH (FRAME_WIDTH/2U)
#define CENTER_HEIGHT (FRAME_HEIGHT/2U)

#define PROPORTIONAL_GAIN (1)
#define INTEGRAL_GAIN (0)
#define DIFFERENTIAL_GAIN (0)

#define PID_MAX_INTEGRAL (100)

#define DEFAULT_SPEED_Y (20)
#define DEFAULT_SPEED_X (30)

// Structures & Enumerations

typedef enum{
	noError_e,
	errorPixyIsBusy_e,
	errorUnknown_e,
	noBlockInSight_e,
	blockFound_e
}Pixy_status_t;

typedef struct{
	uint16_t xCoordinate;
	uint16_t yCoordinate;
} xyStructure_t;

typedef struct{
	int32_t xPID;
	int32_t yPID;
} PID_values;

typedef struct{
	int32_t xError;
	int32_t yError;
} error_values;

// Global Variables

volatile bool g_Adc16ConversionDoneFlag = false;
volatile uint32_t g_Adc16ConversionValue0;
volatile uint32_t g_Adc16ConversionValue1;
uint8_t currentChannelIndex = 0;

uint8_t masterTxData[12] = {0xae,0xc1,0x20,0x02,0xFF,0xFF};
uint8_t masterRxData[20] = {0};



uint32_t error_index = 0;

dspi_master_handle_t g_m_handle;
volatile bool isTransferCompleted = false;
dspi_transfer_t masterXfer;

xyStructure_t CoordinatesFromPixy;

Pixy_status_t block_status;

ftm_config_t ftmInfo;

adc16_config_t ADC_config;
adc16_channel_config_t adc16ChannelSE18ConfigStruct;

ftm_chnl_pwm_signal_param_t ftmParam[2];

int32_t previousVerticalError = 0xFFFF;
int32_t previousHorizontalError = 0xFFFF;
int32_t integralAccumulatorVertical = 0;
int32_t integralAccumulatorHorizontal = 0;

int8_t errorArrayX[300] = {0};
int8_t errorArrayY[300] = {0};

// Local Functions

void InitializePeripherals(void);

void DSPI_MasterUserCallback(SPI_Type *base, dspi_master_handle_t *handle, status_t status, void *userData);

void GetPotentiometerReadings(void);

Pixy_status_t checkPixyForCoordinates(void);

int32_t VerticalPIDCalculation(int32_t error);
int32_t HorizontalPIDCalculation(int32_t error);

// Callbacks

void ADC0_IRQHandler(void)
{
    g_Adc16ConversionDoneFlag = true;
    /* Read conversion result to clear the conversion completed flag. */
    g_Adc16ConversionValue0 = ADC16_GetChannelConversionValue(ADC0, 0);
/* Add for ARM errata 838869, affects Cortex-M4, Cortex-M4F Store immediate overlapping
  exception return operation might vector to incorrect interrupt */
#if defined __CORTEX_M && (__CORTEX_M == 4U)
    __DSB();
#endif
}

void ADC1_IRQHandler(void)
{
    g_Adc16ConversionDoneFlag = true;
    /* Read conversion result to clear the conversion completed flag. */
    g_Adc16ConversionValue1 = ADC16_GetChannelConversionValue(ADC1, 0);
/* Add for ARM errata 838869, affects Cortex-M4, Cortex-M4F Store immediate overlapping
  exception return operation might vector to incorrect interrupt */
#if defined __CORTEX_M && (__CORTEX_M == 4U)
    __DSB();
#endif
}

void DSPI_MasterUserCallback(SPI_Type *base, dspi_master_handle_t *handle, status_t status, void *userData)
{
    if (status == kStatus_Success)
    {
        __NOP();
    }

    isTransferCompleted = true;
}

/*
 * @brief   Application entry point.
 */
int main(void) {

  	/* Init board hardware. */
    BOARD_InitBootPins();
    BOARD_InitBootClocks();
    BOARD_InitBootPeripherals();
  	/* Init FSL debug console. */
    BOARD_InitDebugConsole();

    InitializePeripherals();

    error_values error_coordinates;

    error_coordinates.xError = 0;
    error_coordinates.yError = 0;

    PID_values PID;

    PID.xPID = 0;
    PID.yPID = 0;
    int32_t tempYerror = 0;
    int32_t tempXerror = 0;

    while(1) {

    	block_status = checkPixyForCoordinates();

    	if(blockFound_e == block_status)
    	{
    		GetPotentiometerReadings();
    		PRINTF("Read Successful\n\r");
    		PRINTF("X: %i Y: %i\n\r",CoordinatesFromPixy.xCoordinate,CoordinatesFromPixy.yCoordinate);

    	    error_coordinates.xError = CENTER_WIDTH - CoordinatesFromPixy.xCoordinate;
    	    error_coordinates.yError = CoordinatesFromPixy.yCoordinate - CENTER_HEIGHT;

    	    PID.xPID = HorizontalPIDCalculation(error_coordinates.xError);
    	    PID.yPID = VerticalPIDCalculation(error_coordinates.yError);

    	    PRINTF("X PID:%i Y PID:%i\n\r",PID.xPID,PID.yPID);

    	    if(error_coordinates.yError<0)	// Connected to A1 and B1
    	    {
//    	    	//CCW
    			GPIO_PinWrite(GPIOC,3,1);
    			GPIO_PinWrite(GPIOC,12,0);
    			tempYerror = -error_coordinates.yError;
    			//PID.yPID = PID.yPID * (-1);
    	    }
    	    else
    	    {
//    	    	//CW
    			GPIO_PinWrite(GPIOC,3,0);
    			GPIO_PinWrite(GPIOC,12,1);
    			tempYerror = error_coordinates.yError;
    	    }

    	    if(error_coordinates.xError<0)	// Connected to A2 and B2
    	    {
				//CW
				GPIO_PinWrite(GPIOB, 23, 0);
				GPIO_PinWrite(GPIOC, 4, 1);
				tempXerror = -error_coordinates.xError;
				//PID.yPID = PID.yPID * (-1);
    	    }
    	    else
    	    {
    	    	//CCW
				GPIO_PinWrite(GPIOB, 23, 1);
				GPIO_PinWrite(GPIOC, 4, 0);
				tempXerror = error_coordinates.xError;
    	    }

    	    if(tempXerror<=30)
    	    {
    			GPIO_PinWrite(GPIOB,23,1);
    			GPIO_PinWrite(GPIOC,4,1);
    	    }

    	    if(tempYerror<=20)
    	    {
    			GPIO_PinWrite(GPIOC,3,1);
    			GPIO_PinWrite(GPIOC,12,1);
    	    }

    	    if(PID.yPID > 30)
    	    {
    	    	PID.yPID = 30;
    	    }
    	    else if(PID.yPID<20)
    	    {
    	    	PID.yPID = 20;
    	    }

    	    if(PID.xPID > 30)
    	    {
    	    	PID.xPID = 30;
    	    }
    	    else if(PID.xPID<20)
    	    {
    	    	PID.xPID = 20;
    	    }
//
    		FTM_UpdatePwmDutycycle(FTM0,
    				(ftm_chnl_t) kFTM_Chnl_1, kFTM_EdgeAlignedPwm,
					100 - PID.xPID);	//Corresponds to Motor1
    		FTM_UpdatePwmDutycycle(FTM3,
    				(ftm_chnl_t) kFTM_Chnl_4, kFTM_EdgeAlignedPwm,
					100-PID.yPID);	//Corresponds to Motor2
    		FTM_SetSoftwareTrigger(FTM0, 1);
    		FTM_SetSoftwareTrigger(FTM3, 1);

//    		if((60*5) == error_index)
//    		{
//
//    			for(uint32_t c = 0;c<=300;c++)
//    			{
//    				PRINTF("%i,",errorArrayX[c]);
//    			}
//    			PRINTF("\n\r");
//    			for(uint32_t c = 0;c<=300;c++)
//    			{
//    				PRINTF("%i,",errorArrayY[c]);
//    			}
//    			error_index = error_index;
//    		}
//
//    		errorArrayX[error_index] = error_coordinates.xError;
//    		errorArrayY[error_index] = error_coordinates.yError;
//
//    		error_index++;

    	}
    	else if(noBlockInSight_e == block_status)
    	{
			GPIO_PinWrite(GPIOC,3,1);
			GPIO_PinWrite(GPIOC,12,1);
			GPIO_PinWrite(GPIOB,23,1);
			GPIO_PinWrite(GPIOC,4,1);
    	}
    }
    return 0 ;
}


Pixy_status_t checkPixyForCoordinates(void)
{
	isTransferCompleted    = false;
	masterXfer.txData      = masterTxData;
	masterXfer.rxData      = NULL;
	masterXfer.dataSize    = 12;
	masterXfer.configFlags = kDSPI_MasterCtar0 | kDSPI_MasterPcs0 | kDSPI_MasterPcsContinuous;

	DSPI_MasterTransferNonBlocking(DSPI0, &g_m_handle, &masterXfer);

	/* Wait transfer complete */
	while (!isTransferCompleted)
	{
	}

	/* Start master transfer, receive data from slave */
	isTransferCompleted    = false;
	masterXfer.txData      = NULL;
	masterXfer.rxData      = masterRxData;
	masterXfer.dataSize    = 20;
	masterXfer.configFlags = kDSPI_MasterCtar0 | kDSPI_MasterPcs0 | kDSPI_MasterPcsContinuous;
	DSPI_MasterTransferNonBlocking(DSPI0, &g_m_handle, &masterXfer);

	while (!isTransferCompleted)
	{
	}

	if(PIXY_TYPE_RESPONSE_ERROR == masterRxData[4])
	{
		return errorPixyIsBusy_e;
	}
	else if(CCC_RESPONSE_BLOCKS == masterRxData[4] && 14 == masterRxData[5])
	{
		CoordinatesFromPixy.xCoordinate = masterRxData[10] | (masterRxData[11] << 8);//10 11
		CoordinatesFromPixy.yCoordinate = masterRxData[12] | (masterRxData[13] << 8);//12 13
		return blockFound_e;
	}
	else if(CCC_RESPONSE_BLOCKS == masterRxData[4] && 0 == masterRxData[5])
	{
		return noBlockInSight_e;
	}
	else
	{
		return errorUnknown_e;
	}

}

void InitializePeripherals(void)
{
	EnableIRQ(ADC0_IRQn);
	EnableIRQ(ADC1_IRQn);

	ftmParam[0].chnlNumber = (ftm_chnl_t) kFTM_Chnl_1;
	ftmParam[0].level = kFTM_LowTrue;
	ftmParam[0].dutyCyclePercent = 0U;
	ftmParam[0].firstEdgeDelayPercent = 0U;

	ftmParam[1].chnlNumber = (ftm_chnl_t) kFTM_Chnl_4;
	ftmParam[1].level = kFTM_LowTrue;
	ftmParam[1].dutyCyclePercent = 0U;
	ftmParam[1].firstEdgeDelayPercent = 0U;

	FTM_GetDefaultConfig(&ftmInfo);
	/* Initialize FTM module */
	FTM_Init(FTM0, &ftmInfo);
	FTM_Init(FTM3, &ftmInfo);

	ADC16_GetDefaultConfig(&ADC_config);

	ADC_config.resolution = kADC16_ResolutionSE12Bit;
	ADC_config.referenceVoltageSource = kADC16_ReferenceVoltageSourceVref;

	ADC16_Init(ADC0, &ADC_config);
	ADC16_Init(ADC1, &ADC_config);
	ADC16_EnableHardwareTrigger(ADC0, 0);
	ADC16_EnableHardwareTrigger(ADC1, 0);

	adc16ChannelSE18ConfigStruct.channelNumber = 18;
	adc16ChannelSE18ConfigStruct.enableDifferentialConversion = 0;
	adc16ChannelSE18ConfigStruct.enableInterruptOnConversionCompleted = 1;

	FTM_SetupPwm(FTM0, ftmParam, 1U, kFTM_EdgeAlignedPwm, 976U,
			CLOCK_GetFreq(kCLOCK_BusClk));
	FTM_StartTimer(FTM0, kFTM_SystemClock);

	FTM_SetupPwm(FTM3, &ftmParam[1], 1U, kFTM_EdgeAlignedPwm, 976U,
			CLOCK_GetFreq(kCLOCK_BusClk));
	FTM_StartTimer(FTM3, kFTM_SystemClock);

	/* Start PWM mode with updated duty cycle */
	FTM_UpdatePwmDutycycle(FTM0,
			(ftm_chnl_t) kFTM_Chnl_1, kFTM_EdgeAlignedPwm,
			DEFAULT_SPEED_X);
	FTM_UpdatePwmDutycycle(FTM3,
			(ftm_chnl_t) kFTM_Chnl_4, kFTM_EdgeAlignedPwm,
			DEFAULT_SPEED_Y);
	/* Software trigger to update registers */
	FTM_SetSoftwareTrigger(FTM0, 1);
	FTM_SetSoftwareTrigger(FTM3, 1);

	GPIO_PinWrite(GPIOB,2,1);
	GPIO_PinWrite(GPIOB,3,1);

	dspi_master_config_t default_configuration_master_spi;

	DSPI_MasterGetDefaultConfig(&default_configuration_master_spi);

	default_configuration_master_spi.ctarConfig.baudRate = TRANSFER_BAUDRATE;

	DSPI_MasterInit(DSPI0,&default_configuration_master_spi,CLOCK_GetFreq(DSPI0_CLK_SRC));
	DSPI_MasterTransferCreateHandle(DSPI0, &g_m_handle, DSPI_MasterUserCallback, NULL);

	ADC16_DoAutoCalibration(ADC0);
	ADC16_DoAutoCalibration(ADC1);

}

void GetPotentiometerReadings(void)
{
	g_Adc16ConversionDoneFlag = false;

	ADC16_SetChannelConfig(ADC1, currentChannelIndex, &adc16ChannelSE18ConfigStruct);
    while (!g_Adc16ConversionDoneFlag)
    {
    }
    PRINTF("POT 1: %i\n\r",g_Adc16ConversionValue0);

	g_Adc16ConversionDoneFlag = false;

	ADC16_SetChannelConfig(ADC0, currentChannelIndex, &adc16ChannelSE18ConfigStruct);
    while (!g_Adc16ConversionDoneFlag)
    {
    }
    PRINTF("POT 2: %i\n\r",g_Adc16ConversionValue1);
}

int32_t VerticalPIDCalculation(int32_t error)
{
	int32_t pid;
	if(0xFFFF != previousVerticalError)
	{
		integralAccumulatorVertical += error;
		// bound the integral
		if (integralAccumulatorVertical > PID_MAX_INTEGRAL)
			integralAccumulatorVertical = PID_MAX_INTEGRAL;
		else if (integralAccumulatorVertical < -PID_MAX_INTEGRAL)
			integralAccumulatorVertical = -PID_MAX_INTEGRAL;

		pid = (error*PROPORTIONAL_GAIN + ((integralAccumulatorVertical*INTEGRAL_GAIN)/60) + (error - previousVerticalError)*DIFFERENTIAL_GAIN * 60);

	}
	else
	{
		pid = 0;
	}
	previousVerticalError = error;
	return pid;
}

int32_t HorizontalPIDCalculation(int32_t error)
{
	int32_t pid;
	if(0xFFFF != previousHorizontalError)
	{
		previousHorizontalError += error;
		// bound the integral
		if (integralAccumulatorHorizontal > PID_MAX_INTEGRAL)
			integralAccumulatorHorizontal = PID_MAX_INTEGRAL;
		else if (integralAccumulatorHorizontal < -PID_MAX_INTEGRAL)
			integralAccumulatorHorizontal = -PID_MAX_INTEGRAL;

		pid = (error*PROPORTIONAL_GAIN + ((integralAccumulatorHorizontal*INTEGRAL_GAIN)/60) + (error - previousHorizontalError)*DIFFERENTIAL_GAIN*60);

	}
	else
	{
		pid = 0;
	}
	previousHorizontalError = error;
	return pid;
}

